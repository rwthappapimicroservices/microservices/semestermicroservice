# RWTH Semester

Ein Microservice zum holen des aktuellen Semesters. Dies ist nur ein temporärer Hilfs-Microservice.

## Environment
```bash
SEMESTER_LIST="ws18;ss18;ws17;ss17"
```

## API
- `/AktuellesSemester` - GET
    - Gibt das aktuelle Semester als String aus
