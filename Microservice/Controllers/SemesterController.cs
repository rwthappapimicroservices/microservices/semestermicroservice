using System;
using Microsoft.AspNetCore.Mvc;
using PIT.Labs.ApiResponseLib;

namespace RWTHSemester.Controllers
{
    [ApiController]
    [Route("AktuellesSemester")]
    public class SemesterController : ControllerBase
    {
        [HttpGet]
        public ActionResult<ApiResponse<string>> Get([FromQuery] string semesterOffset)
        {
            try
            {
                string semesters_str = Environment.GetEnvironmentVariable("SEMESTER_LIST");
                int semester_offset_int;
                if (! Int32.TryParse(semesterOffset, out semester_offset_int))
                {
                    semester_offset_int = 0;
                }
                var semesters = semesters_str.Split(";");
                if (semesters.Length <= semester_offset_int) {
                    throw new Exception("Dieses Semester gibts nicht.");
                }
                var output = semesters[semester_offset_int];
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<string>(e);
            }
        }
    }
}
